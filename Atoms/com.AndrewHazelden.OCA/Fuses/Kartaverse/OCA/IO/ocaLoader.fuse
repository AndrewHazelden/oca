-- ============================================================================
-- modules
-- ============================================================================
local json = self and require("dkjson") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "ocaLoader"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = "ScriptVal",
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\OCA\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Reads an OCA datastream from a JSON file.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]

    InFile = self:AddInput("Filename" , "Filename" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = false,
        FC_ClipBrowse = false,
        FCS_FilterString =  "OCA (*.oca)|*.oca|JSON (*.json)|*.json",
        LINK_Main = 1
    })
    InRemoveNonPrintableCharacters = self:AddInput("Remove Non-Printable Characters", "RemoveNonPrintableCharacters", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })
    InSort = self:AddInput("Sort", "Sort", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })
    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })
    OutScriptVal = self:AddOutput("Output", "Output", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function read_file(filename)
    local f = assert(io.open(filename , "rb"))
    local content = f:read("*all")
    f:close()

    return content
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InFile:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)
    local txt_str = read_file(abs_path)

    -- Zap the gremlins found in invisible non-ASCII printable characters
    local remove = InRemoveNonPrintableCharacters:GetValue(req).Value
    if remove == 1 then
        -- txt_str = string.gsub(txt_str, "[%z\1-\31]", "")
        -- txt_str = string.gsub(txt_str, "[%W]", "")
        txt_str = string.gsub(txt_str, "[%z\1-\8\11-\12\14-\31]", "")
        txt_str = string.gsub(txt_str, "[\127-\255]", "")
    end

    -- dump(txt_str)

    local json_str = txt_str
    local sort = InSort:GetValue(req).Value

    local tbl = json.decode(json_str)

    -- Sort the array alphabetically
    if sort == 1.0 then
        table.sort(tbl)
    end

    -- Append extra values
    if tbl then
        tbl.ocaFileName = abs_path
    end

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    OutScriptVal:Set(req, ScriptValParam(tbl))
end
