# Open Cell Animation Data Nodes

## Overview

The OCA [Open Cell Animation](http://oca.rxlab.guide/) data nodes for Blackmagic Design Resolve/Fusion allow you to interact with 2D animation data inside a node graph. This approach works with a series of nodal operators that allow you to edit and modify the OCA data on the fly.

This OCA implementation is based around the "ScriptVal" datatype in Fusion, and works using ideas pioneered by the Vonk Ultra data node project.

![OCA Nodes](images/oca.png)

## Online Documentation

There is a (WIP) guide for the [OCA data nodes available on Google Docs](https://docs.google.com/document/d/1DXnF47CK7dteF7lidwek5-lwy5qB75nBQMt_2Bp0y0g/edit).

## Software Requirements

To run OCA based workflows on your Resolve/Fusion system you will need the following tools:

- BMD Resolve (Free) / Resolve Studio v18.5+
- BMD Fusion Studio
- Reactor Package Manager (Free)
- Vonk Ultra Data Nodes (via Reactor)

## Open Source Software License Term

- GPL v3

## Installation

1. Install the Reactor Package Manager for Resolve/Fusion.

2. Install the Reactor package named "Vonk Ultra" that is found in the "Kartaverse/Vonk Ultra" category in the Reactor window.

3. Install the Reactor package named "Open Cell Animation Data Nodes" that is found in the "Kartaverse/OCA" category in the Reactor window.

4. Quit and relaunch Resolve/Fusion once for the fuse nodes to load.

# OCA Node Categories

The OCA data nodes are separated into the following categories and sub-categories based upon the function they perform:

Create
- ocaStage
- ocaLayer
- ocaFrame
- ocaText

Effects
- ocaLens

Flow
- ocaSwitch
- ocaWireless

IO
- ocaLoader
- ocaSaver

Image
- ocaRender

Utility
- ocaInfo
- ocaMerge
- ocaScript
- ocaReorder (WIP)
- ocaTransform (WIP)
- ocaRename (WIP)
- ocaIsolate (WIP)


## OCA Node Usage

The Fusion node graph allows for the use of "data nodes". This nodal operator driven approach allows the individual node based input and output connections to pass OCA encoded documents "down the flow" in a parametric fashion.

This is achieved by encoding the raw OCA information into a Fusion datatype called a "ScriptVal" which is represented by a Lua table structure. The OCA data is passed between node input and output connections in a way that allows you to visually control the operations that dynamically create, load, edit, render, and save the cell animation.

An OCA node graph starts with either an "ocaLoader" node that imports an existing OCA (JSON formatted) file from disk, or an "ocaStage" node that creates an empty OCA document. 

The "ocaSaver" node writes a modified OCA file back to disk as a JSON document.

The "ocaInfo" node peeks into the contents of the live OCA datastream. This is a handy diagnostic tool.

The "ocaRender" node creates a final "rasterized" rendering of the OCA datastream. Currently this node will create a new image canvas at the defined image resolution, and fill the background with the OCA "backgroundColor" tag value.


